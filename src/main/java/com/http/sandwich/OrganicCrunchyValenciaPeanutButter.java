package com.http.sandwich;


public class OrganicCrunchyValenciaPeanutButter implements PeanutButter {


    @Override
    public void applyToSandwich(Sandwich sandwich, int grams) {
        sandwich.addPeanutButter(grams);
    }
}
