package com.http.sandwich;


import javax.annotation.concurrent.ThreadSafe;

@ThreadSafe
public interface PeanutButter {

    void applyToSandwich(Sandwich sandwich, int grams);

}
